# A Fistful of Sovereigns

![](http://keith-baker.com/wp-content/uploads/2020/12/kbp-frag-announce.png)

A campaign on the Breland/Droaam border, cf. http://keith-baker.com/kbp-threshold/

## Ruleset
- Tales of the Valiant
- 13th Age
- Fate Freeport
?

[Plots & Tropes](Plots.md)

## Notes

Some say [Dragon of Icespire Peak](https://www.dndbeyond.com/sources/doip) and
sequels feel "Wild West". Maybe good to raid for ideas.

Maybe
[Icewind Dale: Rime of the Frostmaiden](https://www.dndbeyond.com/sources/idrotf)
before or after this (incorporating Chris' Norse book) if he doesn't run it.

## Character Creation

### Names
_Firefly RPG_, pp 162, 173, 182, 183, 193

### Why are you in Threshold?

#### Running from something
- Wanted by the law?
- Lost your home?
  - Cyre?
  - Thranish monarchist?
  - etc
- PTSD from the Last War?

#### Running toward something
- Get rich quick scheme?
- Searching for a specific person?

## Every player make up at least 1 NPC, either in _Threshold_ or "back home"
- Note to self, if possible, combine them
