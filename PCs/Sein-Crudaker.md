# Sein Crudaker
- Half [Moon Druid](https://www.dndbeyond.com/classes/druid#CircleoftheMoon)
- Probably from the swamps of the **Shadow Marches**.
- Ideas for Magic Items to give her:
  - Staff of the Python
  - Insignia of Claws
  - Sentinel Shield
  - Pearl of Power
  - Staff of Healing
  - Necklace of Prayer Beads
  - Staff of the Woodlands
  - Staff of Frost
  - Staff of Fire
  - Dragon Scale Armor
  - Dustwalker Boots (from Mage Gate)
  Maybe make one of these [sentient](https://www.youtube.com/watch?v=g1H5oK_GGmI)
