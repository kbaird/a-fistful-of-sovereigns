# Avil

- Dwarf Cleric of the Silver Flame, ~120 years old
- "Fire and brimstone", heretical
- Probably from **Thrane**, but disagrees enough with the Church (or the idea
  of theocracy) to self-exile. He apparently keeps a bag of dirt from home with
  him (perhaps that's his Holy Symbol)?
- Some connection to **Shavarath, the Battleground**. He maybe had an early
  exposure to it when younger, and it's kind of freaked him out. See **Battle**
  below.
  In 922YK,
  **King Kason** of **Breland** brought forth a demon regiment from **Shavarath**
  to supplement his troops. However, during negotiation for their continued use,
  a marilith broke free and killed the king before being banished back to
  **Shavarath** [_tFoW_, pg19&89]
- Maybe give him the [Hammer of Thunderbolts](https://www.dndbeyond.com/magic-items/hammer-of-thunderbolts)
early on, so he has to gather the prereq items to activate it fully. Also refer to Dungeon Dudes'
[Legendary Weapons video](https://www.youtube.com/watch?v=pK5eEaEh378&list=PLQMqiULo_05MEpA_g_0w5GvpOGJ0LitFx&index=2&t=220s).
  - Prereqs
    - https://www.dndbeyond.com/magic-items/belt-of-giant-strength
    - https://www.dndbeyond.com/magic-items/gauntlets-of-ogre-power
  - Name it [Distant Storms Foretold](https://shadowrun-series.fandom.com/wiki/Shadowrun:_Hong_Kong_weapons)

Have [Dagon](../../Plots/Dagon.md), from Ed Greenwood's _Nine Hells Revisted_
article from _Dragon_ 91 offer the location of one of the magic items in exchange
for some ethical compromise, and/or use **The Hornhold Crypts** (_DriveThruRPG.com_).

Perhaps **Dagon** wants to recruit his help in a temporary alliance between the
Devils and Archons (giving it some LG legitimacy), but with the ultimate goal
of encouraging ever-more ethical compromises to corrupt **Avil** and eventually
gain control of his soul and recruit him as a Devil.

## Battle

In 922YK, **King Kason of Breland** summoned demons from
**Shavarath** [_tFoW_, pg19&89] to fight on his side in a battle (unclear who
the other side was, could have been **Thrane**). It went badly, killing him,
various carnage, etc. If **Avil** is old enough (and as a Dwarf, he could be),
he could've been a Thranish private or chaplain or whatever on the other side
of that battle. Assuming 40yo to enlist for Dwarves, he's born ~882YK, making
him ~115-120yo at campaign start. Still pretty young for a Dwarf. Still correct?

> I was thinking that his unit was unknowingly summoned to hell (**Shavarath**),
> where demons possessed his squad.  They were going to return with an assignment
> to do some awful thing and betray the Thranish people in some horrible way.
> So as they slept he killed them silently... a bit of holy wet works.  He
> wandered through hell a bit, and perhaps an angel found him, sent him back.
> When he was sent back, **Avil** found himself within the Mourning tattooed
> with the Marks of Warding.

> He has no idea why he wasn't possessed, why he was saved or given the marks.

> He carries the tags of his squad with his alms box.

> He dreams of flame and assumes it's the Silver Flame.

##

Does "Mark of Warding" mean he is actually a Kundarak Dwarf? Or did you just want
those mechanical effects that we can explain via some other narrative origin?

Mechanical more than anything
- OK

---
"I've run the blade of friendship across the throats of evil and look into eyes
of betrayal as death arrived to claim the damned.  There are only two choices in
life. My purpose is to save who I can and shepherd the rest to the eternity
beyond. There the flames await."
