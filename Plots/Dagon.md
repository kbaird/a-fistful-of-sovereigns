# Plots

## Dagon

Have the PCs (and players) tempted by **Dagon**, from Ed Greenwood's
_Nine Hells Revisited_ article from _Dragon_ 91. Have him appear in
**Threshold** and offer favors, like the location of a useful magic item, in
exchange for increasing compromises:
- Nothing
- Looking the other way when some evil happens
- Doing something that produces both good and evil results
- Doing something with little good result
- Or the cure for a devastating disease is held by an imprisoned evil creature
  who offers it for his release (perhaps with **Dagon** as the creature).
etc.
- Give him Lieutenants over whom he has leverage, making their complicitness
  with his evil plans slightly sympathetic. Maybe the PCs can rescue the
  kidnapped husband, or whatever it is.

Have **Dagon** take the form of
[Celestino Tiro Soares](https://www.dndbeyond.com/characters/33027765) ? Or
perhaps **Soares** has been recruited, **Saruman**-style. Why? **Dagon**
promises to
- restore **Cyre**
- resurrect **Soares**' old partner
- grant a permanent safe home to **New Cyre**
