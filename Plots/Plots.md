# Plots

Work a
[Dhakaani burial barrow](https://www.youtube.com/watch?v=_G2Q1qsoGEU)
into the landscape somehow

Level 1
- Gather medicinal plants
  Use as an excuse to introduce the area, perhaps a group of Druids from whom
  the as-a-wolf murderer will come

Level 2
- Escort Silver Flame pilgrims to a holy site: **The Road to Swordfall**
- First taste of the legendary artifacts and [Dagon](Dagon.md)
  - Maybe retrieve the first from an old Dhakaani? ruin

Level 3
- Mystery: A druid turns into a wolf to murder people to frame a werewolf
  - Refer to
  [Chapter 6: Outsider](https://gamefaqs.gamespot.com/pc/121227-shadowrun-hong-kong/faqs/72578)
  from **Shadowrun Returns: Hong Kong** scenario with **Gaichu**

Level 4
- _Assault of the Steel Horde_ [Kobold Press' **Prepared 2**, pg10]
  - old Brelish war machine

Level 5
- [Boranel's Half-Elven Son](../sharn-noir.git/TODO.md)
  - Maybe **Boranel**'s death bed is at
  [Castle Arakhain](https://eberron.fandom.com/wiki/Castle_Arakhain)

Level 6
- Consider [The Red Hand of Doom](https://www.drivethrurpg.com/product/28797/Red-Hand-of-Doom-3e) , cf.
https://forums.giantitp.com/showthread.php?171284-The-3-5-Red-Hand-Of-Doom-Handbook-for-DMs-Major-spoilers!-WIP-PEACH

At some level, have a dungeon in which previous adventurers/spelunkers who died
there have been made into undead guardians. Start with mindless undead, like
skeletons or zombies, but with armor and other clothing that matches a few
recent eras. Eventually, the PCs encounter intelligent undead, one or more of
whom asks to be released from undeath via some sort of ritual.

## Lich-Focused

A lich breaks up its phylactery and embeds it into various nation's crown
jewels. Perhaps after a monarch dies, it also becomes some sort of undead, and
is a mid-campaign villain, which peels the onion a bit.
(Maybe better-suited to a different campaign)
Refer to DD video at https://youtu.be/MDUJIKmvQEM?t=1735

## Other
- Steal the map idea from _Van Helsing_ (2004 movie) where returning a fragment
  and reading the inscription turns the map into a mirror that functions as a
  portal.
- Spellwards holding back a terrible ancient evil are starting to decay. The PCs
  must gather the parts of a fragmented artifact scattered across Khorvaire and
  piece it back together to restore the failing wards.
- _Invisibility_ wards concealing an arcane tower fail, revealing its existence
  to surprised locals.
- An ancient suit of armor, imbued with the mind of a legendary hero, has one
  last mission to fulfill. It attempts to possess the next person to wear it
  and finish what it started long ago. (Or just asks nicely?)
- Miners find Dhakaani relics
- Guard a caravan
- Somebody comes to town
  - Amnesiac robbery victim
  - Airship crashes near town
  - Bounty hunt for a bank robber
- [Burning Plague](../../../../Documents/RPG/DnD/BurningPlague.pdf)
- "Going Once... Going Twice" from _Dungeon_ Magazine #13
  - Maybe set in in **Ardev** or somewhere else farther into **Breland**
- "Necropolis" from _Dungeon_ Magazine #16
  - **Balfa the Betrayed** was a **Brelish** soldier fighting **Droaamish**
  Gnolls or **Aundairian** soldiers (or **Droaamites** recruited by **Aundair**
  to cause trouble for **Breland**).
- The _Brotherhood of Redemption_ [_Ptolus_, pg106] kidnaps evil creatures and
  forcibly turns them good.

## Specific Movies
- https://en.wikipedia.org/wiki/Western_(genre)

## Stock Western Plots
- https://forum.rpg.net/index.php?threads/101-western-plot-ideas.766397/
- https://tvtropes.org/pmwiki/pmwiki.php/Main/TheSevenWesternPlots
- https://tvtropes.org/pmwiki/pmwiki.php/Main/WildWestTropes

### More specific tropes
- https://tvtropes.org/pmwiki/pmwiki.php/Main/CattleDrive
- Stampede

## Video Games

## Books
- [Mike Resnick's **Weird West Tales**](Resnick.md) (on my Kobo)
