# Mike Resnick's **Weird West Tales** (on my Kobo)

## _Buntline_
- Native magic prevents the USA from being west of Mississippi
- Edison & Buntline electrifying Tombstone, also putting bulletproof brass
  on lots of buildings
- Edison was wounded and got a replacement metal arm
- Geronimo says not responsible & cursed Bat Masterson to be a bat
- Doc H suspects the Clantons (horse thieves), actually Curly Bill Brocius
  under orders from Hook Nose, who suspected Edison of being able to
  overcome Hook Nose's magic

## _The Doctor and the Kid_

## _The Doctor and the Dinosaurs_

## _The Doctor and the Rough Rider_

