# NPC Types

- Cyran or other non-Brelish [Remnant](https://tvtropes.org/pmwiki/pmwiki.php/Main/TheRemnant)
- [Wandslinger](https://tvtropes.org/pmwiki/pmwiki.php/Main/TheGunslinger)
- [House Deneith](https://tvtropes.org/pmwiki/pmwiki.php/Main/USMarshal)
  [Sentinel Marshals](https://eberron.fandom.com/wiki/Sentinel_Marshals)
  - They were granted the right to cross any border by the King of Galifar, but
    when Galifar collapsed into civil war the rulers of the Five Nations agreed
    to let the Sentinel Marshals continue to pursue their prey across all nations,
    to maintain a neutral lawkeeping force that would be respected throughout
    Khorvaire.
- [House Jorasco Frontier Doctor](https://tvtropes.org/pmwiki/pmwiki.php/Main/FrontierDoctor)
- [Brelish (or other) Remittance Man](https://tvtropes.org/pmwiki/pmwiki.php/Main/RemittanceMan)
  - Maybe a distant cousin of the Thranish royal family
- [Southern Gentleman](https://tvtropes.org/pmwiki/pmwiki.php/Main/SouthernGentleman)
  - Former warforged dealer (lean on previous slaveholding)
- **Royal Dar Regiment**, [Dar](https://eberron.fandom.com/wiki/Goblinoid) with
  Brelish citizenship, similar to [Gurkhas](https://en.wikipedia.org/wiki/Gurkha)
- "Miss Bessie" (Bessantiránthe)
  - A Bronze Dragon living in a canyon near the border
- Territory Governor / Imperial Official
  - Maybe a Baron who lives back in **Ardev, Breland**
